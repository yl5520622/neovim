return {
  {
    'nvim-tree/nvim-tree.lua',
    dependencies = {
      'nvim-tree/nvim-web-devicons'
    },
    opts = {
      actions = {
        open_file = {
          window_picker = {
            chars = 'qsdfghjklm'
          }
        }
      },
      git = {
        ignore = false
      }
    },
    keys = {
      { '<leader>n', ':NvimTreeToggle<CR>', { desc = 'Toggle tree' } }
    }
  }
}
