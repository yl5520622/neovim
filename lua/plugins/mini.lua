return {
  'echasnovski/mini.nvim',
  config = function()
    -- automatically close pairs
    require('mini.pairs').setup()

    -- configure mini.surround to act like vim-surround
    require('mini.surround').setup({
      mappings = {
        add = 'ys',
        delete = 'ds',
        find = '',
        find_left = '',
        highlight = '',
        replace = 'cs',
        update_n_lines = ''
      }
    })

    -- gc to comment
    require('mini.comment').setup()

    -- delete buffer without changing windows layout
    require('mini.bufremove').setup()
    vim.keymap.set('n', '<leader>q', ':lua MiniBufremove.wipeout()<CR>', { desc = 'Delete buffer' })

    -- show and remove trailing whitespaces
    require('mini.trailspace').setup()

    vim.api.nvim_create_autocmd('BufWritePre', {
      callback = function()
        require('mini.trailspace').trim()
        require('mini.trailspace').trim_last_lines()
      end
    })

    -- open minimap window
    require('mini.map').setup()
    vim.api.nvim_create_user_command('MiniMapToggle', ':lua MiniMap.toggle()', { desc = 'Toggle file map' })

    -- new text objects
    require('mini.ai').setup()
  end
}
