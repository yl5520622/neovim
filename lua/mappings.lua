vim.keymap.set({ 'n', 'v' }, ',', '<Nop>', { silent = true })
-- clear highlighting of hlsearch
vim.keymap.set('n', '<leader>hl', ':nohlsearch<CR>', { desc = 'Clear highlighting', silent = true })

-- use Ctrl-h/j/k/l to move between windows
vim.keymap.set('n', '<C-h>', '<C-w>h', { desc = 'Go to window on the left' })
vim.keymap.set('n', '<C-j>', '<C-w>j', { desc = 'Go to window on the bottom' })
vim.keymap.set('n', '<C-k>', '<C-w>k', { desc = 'Go to window on top' })
vim.keymap.set('n', '<C-l>', '<C-w>l', { desc = 'Go to window on the right' })

-- managing tabs
vim.keymap.set('n', '<leader>tn', ':tabnew<CR>', { desc = 'Open new tab' })
vim.keymap.set('n', '<leader>tc', ':tabclose<CR>', { desc = 'Close current tab' })
vim.keymap.set('n', '<left>', ':tabprev<CR>', { desc = 'Previous tab' })
vim.keymap.set('n', '<right>', ':tabnext<CR>', { desc = 'Next tab' })


-- remap < and > in visual mode so that the block stays selected
vim.keymap.set('v', '>', '>gv', { desc = 'Indent right' })
vim.keymap.set('v', '<', '<gv', { desc = 'Indent left' })

-- select last pasted text
vim.keymap.set('n', 'gV', '`[v`]', { desc = 'Select last pasted text' })

-- autocenter after search
vim.keymap.set('n', 'n', 'nzz', { desc = 'Go to next occurence', silent = true })
vim.keymap.set('n', 'N', 'Nzz', { desc = 'Go to previous occurence', silent = true })
vim.keymap.set('n', '*', '*zz', { desc = 'Find next', silent = true })
vim.keymap.set('n', '#', '#zz', { desc = 'Find previous', silent = true })
vim.keymap.set('n', '<C-o>', '<C-o>zz', { desc = 'Jump to previous location', silent = true })
vim.keymap.set('n', '<C-i>', '<C-i>zz', { desc = 'Jump to next location', silent = true })

-- insert mode mappings
vim.keymap.set('i', '<S-CR>', '<Esc>O', { desc = 'Insert new line below' })
vim.keymap.set('i', 'II', '<Esc>I', { desc = 'Go to start of line' })
vim.keymap.set('i', 'AA', '<Esc>A', { desc = 'Go to end of line' })
vim.keymap.set('i', '<S-Tab>', '<Right>', { desc = 'Move one character to the right' })

-- diagnostic mappings
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, { desc = 'Go to previous diagnostic message' })
vim.keymap.set('n', ']d', vim.diagnostic.goto_next, { desc = 'Go to next diagnostic message' })
vim.keymap.set('n', '<leader>e', vim.diagnostic.open_float, { desc = 'Open floating diagnostic message' })
-- vim.keymap.set('n', '<leader>q', vim.diagnostic.setloclist, { desc = 'Open diagnostics list' })
