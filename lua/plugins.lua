return {
  -- Useful plugin to show you pending keybinds.
  { 'folke/which-key.nvim', opts = {} },

  {
    'folke/tokyonight.nvim',
    priority = 1000,
    config = function()
      vim.cmd.colorscheme 'tokyonight-night'
    end,
  },

  {
    -- Set lualine as statusline
    'nvim-lualine/lualine.nvim',
    opts = {
      options = {
        theme = 'auto'
      },
    },
  },

  -- highlight word under cursor using lsp
  'RRethy/vim-illuminate',

  -- unix shell commands
  'tpope/vim-eunuch',

  -- Detect tabstop and shiftwidth automatically
  'tpope/vim-sleuth',

  -- Write files using sudo
  'lambdalisue/suda.vim',

  -- scrollbar
  { 'dstein64/nvim-scrollview', opts = {} },

  {
    -- Add indentation guides even on blank lines
    'lukas-reineke/indent-blankline.nvim',
    opts = {
      char = '┊',
      show_trailing_blankline_indent = false,
    },
  },

  -- improve neovim's marks
  { 'chentoast/marks.nvim', opts = {} },

  -- runtime for nvim-qt
  'equalsraf/neovim-gui-shim',

  -- easymotion replacement
  'ggandor/lightspeed.nvim',


  -- UI for tree of changes
  {
    'mbbill/undotree',
    keys = {
      { '<leader>u', ':UndotreeToggle<CR>', { desc = 'Toggle undo tree' } }
    },
  },

  -- multiple cursors
  'mg979/vim-visual-multi',

  -- terraform
  'hashivim/vim-terraform',

  -- tagbar alternative
  {
    'liuchengxu/vista.vim',
    keys = {
      { '<leader>t', ':Vista show<CR>', { desc = 'Open vista window' }}
    }
  }
}
