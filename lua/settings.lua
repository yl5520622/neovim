-- set leader to  ,
vim.g.mapleader = ','
vim.g.maplocalleader = ','

-- disable netrw
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

-- General {
vim.o.autoread = true                   -- load file from disk if changed
vim.o.clipboard = 'unnamedplus'
vim.o.mouse = 'a'                       -- use mouse everywhere
vim.o.swapfile = false                  -- no swap files
vim.o.undofile = true                   -- save undo history
vim.o.wildignore = '*.swp,*~,*.pyc,*.o' -- ignore those files
-- }

-- UI {
vim.o.cursorcolumn = true   -- highlight current column
vim.o.cursorline = true     -- highlight current column
vim.o.number = true         -- show line numbers
vim.o.numberwidth = 5       -- width of line numbers
vim.o.ruler = true          -- show line and column number of cursor
vim.o.showcmd = true        -- show command being typed
vim.o.showmatch = true      -- show matching characters
vim.wo.signcolumn = 'yes'   -- keep signcolumn on by default
vim.o.splitright = true     -- open vertical splits on the right
-- }

-- Text formatting/layout {
vim.o.expandtab = true      -- expand tabs
vim.o.wrap = false          -- don't wrap lines
vim.o.linebreak = true      -- when wrap is enabled, break at word
vim.o.shiftround = true     -- round indent ot multiple of shiftwidth
vim.o.shiftwidth = 4        -- autoindent size
vim.o.softtabstop = 4       -- size of an expanded tab
vim.o.tabstop = 4           -- size of real tabs
-- }

-- Search {
vim.o.ignorecase = true     -- ignore case when searching
vim.o.smartcase = true      -- don't ignore case if there's uppercase in patter
-- }

-- Decrease update time
vim.o.updatetime = 250
vim.o.timeout = true
vim.o.timeoutlen = 300

-- Set completeopt to have a better completion experience
vim.o.completeopt = 'menuone,noselect'

-- 24 bit colors in terminal
vim.o.termguicolors = true
